package org.javaforever.infinity.core;

import org.javaforever.infinity.domain.JavascriptMethod;

public interface JQueryPositions {
	public  JavascriptMethod generateJQueryActionMethod() throws Exception;
	public  String generateJQueryActionString() throws Exception;
	public  String generateJQueryActionStringWithSerial() throws Exception;

}
