package org.javaforever.infinity.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.infinity.complexverb.Assign;
import org.javaforever.infinity.complexverb.ListMyActive;
import org.javaforever.infinity.complexverb.ListMyAvailableActive;
import org.javaforever.infinity.complexverb.Revoke;
import org.javaforever.infinity.complexverb.TwoDomainVerb;
import org.javaforever.infinity.core.Verb;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;
import org.javaforever.infinity.verb.ListActive;

public class JsonJspManyTemplate extends JspTemplate {
	protected Domain master;
	protected Domain slave;
	protected String standardName;
	protected String label;
	protected Set<Verb> verbs = new TreeSet<Verb>();
	protected Set<TwoDomainVerb> twoDomainVerbs = new TreeSet<TwoDomainVerb>();
	protected List<Domain> menuDomainList = new ArrayList<Domain>();

	public JsonJspManyTemplate(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.standardName = "link"+master.getCapFirstDomainName()+slave.getCapFirstDomainName();
		this.verbs.add(new ListActive(master));
		this.twoDomainVerbs.add(new ListMyActive(master,slave));
		this.twoDomainVerbs.add(new ListMyAvailableActive(master,slave));
		this.twoDomainVerbs.add(new Assign(master,slave));
		this.twoDomainVerbs.add(new Revoke(master,slave));
	}
	
	public String generateJspString(){
		return generateStatementList().getContent();
	}
	
	@Override
	public StatementList generateStatementList() {
		try {
			Assign assign = new Assign(this.master,this.slave);
			Revoke revoke = new Revoke(this.master,this.slave);
			ListMyActive listMyActive = new ListMyActive(this.master,this.slave);
			ListMyAvailableActive listMyAvailableActive = new ListMyAvailableActive(this.master,this.slave);
			
			List<Writeable> sList =  new ArrayList<Writeable>();
			sList.add(new Statement(1000L,0,"<%@page contentType=\"text/html\" pageEncoding=\"UTF-8\"%>"));
			sList.add(new Statement(2000L,0,"<!DOCTYPE html>"));
			sList.add(new Statement(3000L,0,"<html>"));
			sList.add(new Statement(4000L,0,"<head>"));
			sList.add(new Statement(5000L,0,"<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"));
			sList.add(new Statement(6000L,0,"<title>"+StringUtil.capFirst(this.getStandardName())+" Info.</title>"));
			sList.add(new Statement(7000L,0,"<link href=\"../css/default.css\" rel=\"stylesheet\" type=\"text/css\" />"));
			sList.add(new Statement(8500L,0,"<script type=\"text/javascript\" src=\"../js/jquery-1.11.1.min.js\"></script>"));
			sList.add(new Statement(9000L,0,"</head>"));  
			sList.add(new Statement(10000L,0,"<body>"));
			sList.add(new Statement(11000L,1,"<div id=\"wrapper\">"));
			sList.add(new Statement(12000L,1,"<jsp:include page=\"../jsoninclude/header.jsp\" />"));
			sList.add(new Statement(13000L,1,"<!-- end div#header -->"));
			sList.add(new Statement(14000L,1,"<div id=\"page_wide\">"));
			sList.add(new Statement(15000L,1,"<div id=\"mycontent\">"));
			sList.add(new Statement(16000L,1,"<div id=\"welcome\">"));
			sList.add(new Statement(17000L,1,"<!-- Fetch Rows -->"));
			
			sList.add(new Statement(18000L,1,"<table height=\"800px\" style=\"margin-left:200px\">"));
			sList.add(new Statement(19000L,1,"<tr height=\"30px\">"));
			sList.add(new Statement(20000L,1,"<td>"+this.master.getCapFirstPlural()+"</td>"));
			sList.add(new Statement(21000L,1,"<td>Current "+this.slave.getCapFirstPlural()+"</td>"));
			sList.add(new Statement(22000L,1,"<td>Operation</td>"));
			sList.add(new Statement(23000L,1,"<td>Available "+this.slave.getCapFirstPlural()+"</td>"));
			sList.add(new Statement(24000L,1,"</tr>"));
			sList.add(new Statement(25000L,1,"<tr>"));
			sList.add(new Statement(26000L,1,"<td><select multiple=\"multiple\" id=\"my"+this.master.getCapFirstDomainName()+"\" name=\"my"+this.master.getCapFirstDomainName()+"\" style=\"width:240px;height:700px\" onclick=\"checkMyRow()\"></select></td>"));
			sList.add(new Statement(27000L,1,"<td><select multiple=\"multiple\" id=\"my"+this.slave.getCapFirstPlural()+"\" name=\"my"+this.slave.getCapFirstPlural()+"\" style=\"width:240px;height:700px\"></select></td>"));
			sList.add(new Statement(28000L,1,"<td><input type=\"button\" value=\"&gt;&gt\" onclick=\""+StringUtil.lowerFirst(revoke.getVerbName())+"()\"/><br/>&nbsp;<br/>&nbsp;<br/><input type=\"button\" value=\"&lt;&lt;\" onclick=\""+StringUtil.lowerFirst(assign.getVerbName())+"()\"/></td>"));
			sList.add(new Statement(29000L,1,"<td><select multiple=\"multiple\" id=\"myAvailable"+this.slave.getCapFirstPlural()+"\" name=\"myAvailable"+this.slave.getCapFirstPlural()+"\" style=\"width:240px;height:700px\"></select></td>"));
			sList.add(new Statement(30000L,1,"</tr>"));
			sList.add(new Statement(31000L,1,"</table>"));

			sList.add(new Statement(32000L,1,"</div>"));
			sList.add(new Statement(33000L,1,"<!-- end div#welcome -->"));
			sList.add(new Statement(34000L,1,"</div>"));
			sList.add(new Statement(35000L,1,"<!-- end div#content -->"));
			sList.add(new Statement(36000L,1,"<div id=\"sidebar\">"));   
			sList.add(new Statement(37000L,1,"<ul>"));
			sList.add(new Statement(38000L,1,"<jsp:include page=\"../jsoninclude/jsonusernav.jsp\"/>"));
			sList.add(new Statement(39000L,1,"<!-- end navigation -->"));
			sList.add(new Statement(40000L,1,"<jsp:include page=\"../jsoninclude/updates.jsp\"/>"));
			sList.add(new Statement(41000L,1,"<!-- end updates -->"));
			sList.add(new Statement(42000L,1,"</ul>"));                   
			sList.add(new Statement(43000L,1,"</div>"));
			sList.add(new Statement(44000L,1,"<!-- end div#sidebar -->"));
			sList.add(new Statement(45000L,1,"<div style=\"clear: both; height: 1px\"></div>"));
			sList.add(new Statement(46000L,1,"</div>"));
			sList.add(new Statement(47000L,1,"<jsp:include page=\"../jsoninclude/footer.jsp\"/>"));
			sList.add(new Statement(48000L,1,"</div>"));        
			sList.add(new Statement(49000L,1,"<!-- end div#wrapper -->"));
			
			sList.add(new Statement(50000L,1,"<script text=\"text/javascript\">"));			
			sList.add(new Statement(51000L,0,"var currentIndex = 0;"));
			Var currentIndex = new Var("currentIndex", new Type("var"));
			Var data = new Var("data",new Type("var"));
			Var myMaster = new Var("my"+this.master.getCapFirstDomainName(), new Type("var"));
			Var componentId = new Var("componentId", new Type("var"));
			sList.add(NamedJavascriptBlockGenerator.generateDocumentReadyCallListMyActiveMastersBlock(this.master).generateBlockStatementList(51500L,0));
			sList.add(NamedJavascriptMethodGenerator.generateCheckMyRowMethod(this.master,this.slave,currentIndex).generateMethodStatementListIncludingContainer(52000L,0));
			sList.add(NamedJavascriptMethodGenerator.generateLoadMyMasters(this.master,this.slave,currentIndex).generateMethodStatementListIncludingContainer(53000L,0));
			
			//sList.add(assign.generateEasyUIActionMethod().generateMethodStatementListIncludingContainer(54000L,0));
			//sList.add(revoke.generateJQueryActionMethod().generateMethodStatementListIncludingContainer(55000L,0));
			sList.add(listMyActive.generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(56000L,0));
			sList.add(listMyAvailableActive.generateEasyUIJSActionMethod().generateMethodStatementListIncludingContainer(57000L,0));
			sList.add(NamedJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementListIncludingContainer(58000L,0));
			sList.add(NamedJavascriptMethodGenerator.generateGenerateDomainOptions(this.master,data,currentIndex,myMaster).generateMethodStatementListIncludingContainer(59000L,0));
			sList.add(NamedJavascriptMethodGenerator.generateGenerateDomainOptionsWithoutSelect(this.slave,data,componentId).generateMethodStatementListIncludingContainer(60000L,0));
			sList.add(NamedJavascriptMethodGenerator.generateCheckMaster(this.master,currentIndex,myMaster).generateMethodStatementListIncludingContainer(61000L,0));
			
			sList.add(new Statement(62000L,1,"</script>"));
			sList.add(JSPNamedStatementGenerator.getBodyEndStatement(63000L, 0));
			sList.add(JSPNamedStatementGenerator.getHtmlEndStatement(64000L, 0));

			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			return null;
		}
	}
	
	@Override
	public String getStandardName() {
		return "link"+master.getCapFirstDomainName()+slave.getCapFirstDomainName();
	}
}
