package org.javaforever.infinity.utils;

import org.javaforever.infinity.domain.Util;

public class StringUtilGenerator extends Util{
	public StringUtilGenerator(){
		super();
		super.fileName = "StringUtil.java";
	}
	
	public StringUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken+".utils");
		super.fileName = "StringUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StringBuilder sb = new StringBuilder();
		sb.append("package "+this.getPackageToken()+";\n");
		sb.append("\n");
		sb.append("public class StringUtil {\n");
		sb.append("public static boolean isBlank(Object o){\n");
	    sb.append("if (o==null || \"\".equals(o)) return true;\n");
		sb.append("else return false;\n");
		sb.append("}\n");
		sb.append("}\n");

		return sb.toString();
	}

}
